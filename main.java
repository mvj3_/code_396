   1. // 设置光源的方向  
   2. float[] direction = new float[]{ 1, 1, 1 };  
   3. //设置环境光亮度  
   4. float light = 0.4f;  
   5. // 选择要应用的反射等级  
   6. float specular = 6;  
   7. // 向mask应用一定级别的模糊  
   8. float blur = 3.5f;  
   9. EmbossMaskFilter emboss=new EmbossMaskFilter(direction,light,specular,blur);  
  10.  
  11. // 应用mask  
  12. myPaint.setMaskFilter(emboss); 